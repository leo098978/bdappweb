//Invocamos a la conexion de la DB
const conexion = require('../database/db');
//LOGIN 

exports.login =   (req,res)=> {
    try{
        
        const {user, password} = req.body;
        if( !user || !password) {
            console.log('Ingrese Datos')
             return res.render('login', { message:'Ingrese Datos'
                
            });  
           
        }
        conexion.query('SELECT * FROM users WHERE user = ? AND password=?', [user,password],  (error, results) =>{
            
            
            if(results==0){
             console.log('Usuario o contraseña incorrectos')
             res.render('login',{
                    message: 'Usuario o contraseña incorrectos'
                });  
                

            }else{
                res.redirect('/');
                
            }

        } );

    }catch (error){
        console.log(error);
    }
};
//GUARDAR un REGISTRO
exports.save = (req, res)=>{
    try{
    const user = req.body.user;
    const password=req.body.password;
    const rol = req.body.rol;
    if( !user || !password || !rol) {
        console.log('Ingrese Datops')
        return res.render('create', { message:'Ingrese Datos'     
       });        
   }
   conexion.query('SELECT * FROM users WHERE user = ?', [user,password],  (error, result) =>{
       
       if (result==0){
        conexion.query('INSERT INTO users SET ?',{user:user,password:password, rol:rol}, (error, results)=>{
            if(error){
                console.log(error);
            }else{
                //console.log(results);   
                res.redirect('/');         
            }
    }); 

       }else{console.log('Usuario ya existe')
        res.render('create',{
            message: 'Usuario ya existe'
        });  
       }
   })
    }catch (error){
        console.log(error);
    }
    
};
//ACTUALIZAR un REGISTRO
exports.update = (req, res)=>{
    const id = req.body.id;
    const user = req.body.user;
    const password = req.body.password;
    const rol = req.body.rol;
    conexion.query('UPDATE users SET ? WHERE id = ?',[{user:user,password:password, rol:rol}, id], (error, results)=>{
        if(error){
            console.log(error);
        }else{           
            res.redirect('/');         
        }
});
};

